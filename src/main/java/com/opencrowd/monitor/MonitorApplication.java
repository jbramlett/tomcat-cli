package com.opencrowd.monitor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * Main entry point for our monitor
 * @author jbramlett
 */
@ComponentScan
@EnableAutoConfiguration
public class MonitorApplication {

	/**
	 * Constructor
	 */
	public MonitorApplication() {
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// create our spring app
		SpringApplication app = new SpringApplication(MonitorApplication.class);
		app.setShowBanner(false);
		app.run(args);
	}
}
