package com.opencrowd.monitor.amqp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.opencrowd.monitor.heartbeat.listener.HeartbeatReceiver;

/**
 * Class containing our initialization of our queue interaction
 * @author jbramlett
 */
//@Component
public class AqmpInitializer {
	private static final Logger logger = LoggerFactory.getLogger(AqmpInitializer.class);
	
	@Autowired
	RabbitTemplate rabbitTemplate;

	@Value("${rabbit.queue.name}")
	String queueName;
	
	@Value("${rabbit.topic.exchange.name}")
	String topicExchangeName;
	
	/**
	 * Creates our bean that represents our queue
	 * @return Queue The queue
	 */
	@Bean
    Queue queue() {
		logger.info("Initializing queue " + queueName);
        return new Queue(queueName, false);
    }

	/**
	 * Creates our topic
	 * @return TopicExchange The topic
	 */
    @Bean
    TopicExchange exchange() {
    	logger.info("Initializing exchange " + topicExchangeName);
        return new TopicExchange(topicExchangeName);
    }

    /**
     * Binds our queue to the given topic
     * @param queue The queue
     * @param exchange The topic
     * @return Binding The new binding
     */
    @Bean
    Binding binding(Queue queue, TopicExchange exchange) {
    	logger.info("Binding " + topicExchangeName + " with queue " + queueName);
        return BindingBuilder.bind(queue).to(exchange).with(queueName);
    }

    /**
     * Establishes our container bean
     * @param connectionFactory The connection factory
     * @param listenerAdapter The listener
     * @return SimpleMessageListenerContainer The message container
     */
    @Bean
    SimpleMessageListenerContainer container(ConnectionFactory connectionFactory, MessageListenerAdapter listenerAdapter) {
        logger.info("Establishing our listener");
    	SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(queueName);
        container.setMessageListener(listenerAdapter);
        return container;
    }

    /**
     * Gets our underlying receiver
     * @return HeartbeatReceiver The underlying receiver
     */
    @Bean
    HeartbeatReceiver heartBeatReceiver() {
    	logger.info("Constructing our HeartbeatReceiver");
        return new HeartbeatReceiver();
    }

    /**
     * Gets our listener adapter
     * @param receiver The underlying receiver
     * @return MessageListenerAdapter The listener adapter
     */
    @Bean
    MessageListenerAdapter listenerAdapter(HeartbeatReceiver receiver) {
    	logger.info("Constructing our MessageListenerAdapter");
        return new MessageListenerAdapter(receiver, "receiveMessage");
    }
}
