package com.opencrowd.monitor.cli;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;

import jline.console.ConsoleReader;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.reflections.Reflections;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.opencrowd.monitor.cli.functions.AbstractFunction;

/**
 * Command line interface for our monitor app
 * @author jbramlett
 */
@Component
public class MonitorCLI implements ApplicationListener<ContextRefreshedEvent> {
	protected ApplicationContext applicationContext;
	protected Map<String, AbstractFunction> functions = new HashMap<String, AbstractFunction>();
	protected Set<String> functionNames = new HashSet<String>();
    protected List<String> history = new ArrayList<String>();
    protected ConsoleReader console;
	
	/**
	 * Constructor
	 */
	public MonitorCLI() {
		super();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	/* (non-Javadoc)
	 * @see org.springframework.context.ApplicationListener#onApplicationEvent(org.springframework.context.ApplicationEvent)
	 */
	public void onApplicationEvent(ContextRefreshedEvent event) {
		this.applicationContext = event.getApplicationContext();
		
		try {
			// create our console
			boolean startLoop = false;
			if (console == null) {
				console = new ConsoleReader();
				console.setPrompt("cmd> ");
				startLoop = true;
			}
	
			initFunctions(this.applicationContext);
			
			if (startLoop) {
				startCLI();
			}
		}
		catch (Exception e) {
			println("Failed to start our command-line-interface!\n" + ExceptionUtils.getStackTrace(e));
		}
	}

	/**
	 * Starts our command line interface
	 */
	private void startCLI() {
		Executors.newSingleThreadExecutor().execute(new Runnable() {
			public void run() {
				runCLI();
			}
		});
	}
	
	/**
	 * Initializes the set of available functions
	 * @param applicationContext The application context
	 */
	private void initFunctions(ApplicationContext applicationContext) {
		functions.clear();
		functionNames.clear();
		
        Reflections reflections = new Reflections("com.opencrowd.monitor.cli.functions");
		Set<Class<? extends AbstractFunction>> discoveredFunctions = reflections.getSubTypesOf(AbstractFunction.class);
		for (Class<? extends AbstractFunction> cls : discoveredFunctions) {
			try {
			    AbstractFunction f = cls.newInstance();
			    f.initialize(applicationContext);
			    f.setConsole(console);
			    f.setHistory(history);
			    functions.put(f.getFunction().toLowerCase(), f);
			    functionNames.add(f.getFunction());
			}
			catch (Throwable t) {
				println("Failed to initialize " + cls.getName() + " - function will not be available");
			}
		}
	}
	
	/**
	 * Prints a message to our console
	 * @param msg The message we are printing
	 */
	private void println(String msg) {
		try {
			if (console != null) {
				console.println(msg);
			}
		}
		catch (Throwable t) {
			// do nothing;
		}
	}
	
	/**
	 * Runs our command line interface
	 */
	public void runCLI() {
		try {            		
            // now put us in a loop processing incoming commands
			while (true) {
				String command = console.readLine();
				
				String[] commandArgs = command.split(" ");
				String commandName = commandArgs[0].toLowerCase();
				
				if (StringUtils.equalsIgnoreCase("shutdown", commandName) || StringUtils.equalsIgnoreCase("quit", commandName) || StringUtils.equalsIgnoreCase("exit", commandName)) {
					break;
				}
				else if (StringUtils.equalsIgnoreCase("help", commandName)) {
				    console.println("Available commands:\nshutdown/quit/exit\n" + StringUtils.join(functionNames, "\n"));		    
				}
				else if (functions.containsKey(commandName)) {
			        AbstractFunction function = functions.get(commandName);
			        function.execute(command);
			    }
			    else {
			        console.println("Invalid command: " + command);
			    }
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if (applicationContext != null) {
				SpringApplication.exit(applicationContext, new ExitCodeGenerator() {
					public int getExitCode() {
						return 0;
					}					
				});
			}
			
			// force an exit as our rabbit mq seems to be leaving listeners
			System.exit(0);
		}
	}
}
