package com.opencrowd.monitor.cli.functions;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import jline.console.ConsoleReader;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.springframework.context.ApplicationContext;

/**
 * Base class for a function that can be wired in to the
 * SearchServiceInABox
 * 
 * @author d305890
 */
public abstract class AbstractFunction {

    protected ConsoleReader console;
    protected List<String> history;
    protected String function;
    protected Options options;
    protected boolean isHelp = false;
    protected CommandLineParser parser = new BasicParser();

    /**
     * Constructor
     */
    public AbstractFunction(String function) {
        this.function = function;
        options = buildOptions();
    }
    
    /**
     * Initializes our instance
     * @param applicationContext The spring application we are running
     */
    public void initialize(ApplicationContext applicationContext) {
    }
    
    /**
     * Sets the console
     * @param console the console to set
     */
    public void setConsole(ConsoleReader console) {
        this.console = console;
    }

    /**
     * Sets the history
     * @param history the history to set
     */
    public void setHistory(List<String> history) {
        this.history = history;
    }

    /**
     * Gets the function name
     * @return String The function name
     */
    public String getFunction() {
        return function;
    }
    
    /**
     * Builds our set of options
     * @return Options The functions options
     */
    protected abstract Options buildOptions();
    
    /**
     * Gets the options for this function
     * @return Options The options for this function
     */
    public Options getOptions() {
        if (options == null) {
            options = buildOptions();
        }
        return options;
    }
    
    /**
     * Parses the command line args
     * @param commandLineArgs The command line args
     */
    public void parseCommandLine(String[] commandLineArgs) throws Exception {
        CommandLine commandLine = parser.parse(getOptions(), commandLineArgs);        
        parseCommandLine(commandLine);
        
        // now see if this is a help
        setIsHelp(commandLine);
    }
    
    /**
     * Parses our command line
     * @param commandLine The command line
     */
    protected abstract void parseCommandLine(CommandLine commandLine);
    
    /**
     * Returns a flag indicating if this instance if valid
     * @return boolean Returns true if this instance is valid
     */
    public abstract boolean isValid();

    /**
     * Sets our flag indicating if this is a help request
     * @param commandLine The parsed command line
     */
    protected void setIsHelp(CommandLine commandLine) {
        isHelp = commandLine.hasOption("help");
    }
    
    /**
     * Returns a flag indicating if our command line parsed to a help request
     * @return
     */
    public boolean isHelp() {
        return isHelp;
    }
    
    /**
     * Prints our help string
     */
    public String printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        formatter.printUsage(pw, 160, function, getOptions());
        return sw.toString();
    }
    
    /**
     * Executes this command
     * @param command The command being executed
     */
    public void execute(String command) {
        String[] commandArgs = command.split(" ");
        try {
            parseCommandLine(commandArgs);
            if (!isValid() || isHelp()) {
                println(printHelp());
            }
            else {
                if (addToHistory() && history != null) {
                    history.add(command);
                }
                internalExecute();
            }
        }
        catch (Throwable t) {
            println("Failed executing " + command, t);
        }

    }
    
    /**
     * Returns a flag indicating if this command should be tracked in our history
     * @return boolean Returns true if the command should be tracked, false otherwise
     */
    protected boolean addToHistory() {
        return true;
    }
    
    /**
     * Writes a message to the console
     * @param msg The message we are writing
     */
    protected void println(String msg) {
        if (console != null) {
            try {
                console.println(msg);
            }
            catch (IOException ioe) {                
            }
        }        
        else {
            System.out.println(msg);
        }
    }
    /**
     * Writes a message to the console
     * @param msg The message we are writing
     * @param t The exception
     */
    protected void println(String msg, Throwable t) {
        if (console != null) {
            try {
                console.println(msg);
                if (t != null) {
                    t.printStackTrace(new PrintWriter(console.getOutput()));    
                }
            }
            catch (IOException ioe) {                
            }
        }        
        else {
            System.out.println(msg);
            if (t != null) {
                t.printStackTrace();
            }
        }
    }
    
    /**
     * Executes this function
     */
    public abstract void internalExecute() throws Exception;
    
    /**
     * Verifies the given output directory exists, and creates it
     * if it does not
     * @param outDir The directory we are creating
     */
    protected void createDirectory(String outDir) {
        File f = new File(outDir);
        if (!f.exists()) {
            f.mkdirs();
        }
    }
    
	/**
	 * Gets a bean from the given app context - if our config contains a bean name then we use that
	 * otherwise it is just looked up by class
	 * @param ctx The app context
	 * @param beanName The name of the bean we are looking up (can be null)
	 * @param cls The class of the bean we are creating
	 * @return T The bean instance
	 */
	protected <T> T findBean(ApplicationContext ctx, String beanName, Class<T> cls) {
		
		if (beanName != null) {
			if (ctx.containsBean(beanName)) {
				return ctx.getBean(beanName, cls);
			}
			else {
				return ctx.getBean(cls);
			}
		}
		else {
			return ctx.getBean(cls);
		}
	}    
}
