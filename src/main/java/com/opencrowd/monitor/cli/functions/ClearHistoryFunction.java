package com.opencrowd.monitor.cli.functions;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;

/**
 * Clears our history of commands executed
 * @author d305890
 */
public class ClearHistoryFunction extends AbstractFunction {
    private static final String kFunction = "clrHistory";
    
    /**
     * Constructor
     */
    public ClearHistoryFunction() {
        super(kFunction);
    }

    /* (non-Javadoc)
     * @see com.jpmorgan.search.inabox.functions.AbstractFunction#buildOptions()
     */
    @Override
    protected Options buildOptions() {
        Options options = new Options();
        options.addOption("help", false, "This message");
        
        return options;
    }

    /* (non-Javadoc)
     * @see com.jpmorgan.search.inabox.functions.AbstractFunction#parseCommandLine(org.apache.commons.cli.CommandLine)
     */
    @Override
    protected void parseCommandLine(CommandLine commandLine) {
    }

    /* (non-Javadoc)
     * @see com.jpmorgan.search.inabox.functions.AbstractFunction#isValid()
     */
    @Override
    public boolean isValid() {
        return true;
    }

    /* (non-Javadoc)
     * @see com.jpmorgan.search.inabox.functions.AbstractFunction#internalExecute()
     */
    @Override
    public void internalExecute() throws Exception {
        history.clear();
    }
    /* (non-Javadoc)
     * @see com.jpmorgan.search.inabox.functions.AbstractFunction#addToHistory()
     */
    @Override
    protected boolean addToHistory() {
        return false;
    }

}
