package com.opencrowd.monitor.cli.functions;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.apache.commons.lang3.StringUtils;

/**
 * Prints the history of commands executed
 * @author d305890
 */
public class HistoryFunction extends AbstractFunction {
    private static final String kFunction = "history";
    
    /**
     * Constructor
     */
    public HistoryFunction() {
        super(kFunction);
    }

    /* (non-Javadoc)
     * @see com.jpmorgan.search.inabox.functions.AbstractFunction#buildOptions()
     */
    @Override
    protected Options buildOptions() {
        Options options = new Options();
        options.addOption("help", false, "This message");
        
        return options;
    }

    /* (non-Javadoc)
     * @see com.jpmorgan.search.inabox.functions.AbstractFunction#parseCommandLine(org.apache.commons.cli.CommandLine)
     */
    @Override
    protected void parseCommandLine(CommandLine commandLine) {
    }

    /* (non-Javadoc)
     * @see com.jpmorgan.search.inabox.functions.AbstractFunction#isValid()
     */
    @Override
    public boolean isValid() {
        return true;
    }

    /* (non-Javadoc)
     * @see com.jpmorgan.search.inabox.functions.AbstractFunction#internalExecute()
     */
    @Override
    public void internalExecute() throws Exception {
        println("History:\n" + StringUtils.join(history, "\n"));
    }

    /* (non-Javadoc)
     * @see com.jpmorgan.search.inabox.functions.AbstractFunction#addToHistory()
     */
    @Override
    protected boolean addToHistory() {
        return false;
    }

}
