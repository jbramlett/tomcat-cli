package com.opencrowd.monitor.cli.functions;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.ApplicationContext;

/**
 * Sends a command over rabbitmq
 * @author jbramlett
 */
public class SendRabbitCommandFunction extends AbstractFunction {
	private static final String kFunction = "sendRabbit";
	
	private String msg = null;
	private String queue = null;
	
	RabbitTemplate rabbitTemplate;

	
	/**
	 * Constructor
	 */
	public SendRabbitCommandFunction() {
		super(kFunction);
	}

	/* (non-Javadoc)
	 * @see com.opencrowd.monitor.cli.functions.AbstractFunction#buildOptions()
	 */
	@Override
	protected Options buildOptions() {
        Options options = new Options();
        options.addOption("help", false, "This message");
        options.addOption("msg", true, "The message to send");
        options.addOption("queue", true, "The queue to send the message to");
        return options;
	}

	/* (non-Javadoc)
	 * @see com.opencrowd.monitor.cli.functions.AbstractFunction#parseCommandLine(org.apache.commons.cli.CommandLine)
	 */
	@Override
	protected void parseCommandLine(CommandLine commandLine) {
		if (commandLine.hasOption("msg")) {
			msg = commandLine.getOptionValue("msg");
		}
		if (commandLine.hasOption("queue")) {
			queue = commandLine.getOptionValue("queue");
		}		
	}

	/* (non-Javadoc)
	 * @see com.opencrowd.monitor.cli.functions.AbstractFunction#initialize(org.springframework.context.ApplicationContext)
	 */
	@Override
	public void initialize(ApplicationContext applicationContext) {
		super.initialize(applicationContext);
		
		rabbitTemplate = findBean(applicationContext, "rabbitTemplate", RabbitTemplate.class); 		
	}

	/* (non-Javadoc)
	 * @see com.opencrowd.monitor.cli.functions.AbstractFunction#isValid()
	 */
	@Override
	public boolean isValid() {
		return msg != null && queue != null && rabbitTemplate != null;
	}

	/* (non-Javadoc)
	 * @see com.opencrowd.monitor.cli.functions.AbstractFunction#internalExecute()
	 */
	@Override
	public void internalExecute() throws Exception {
		rabbitTemplate.convertAndSend(queue, msg);
	}

}
