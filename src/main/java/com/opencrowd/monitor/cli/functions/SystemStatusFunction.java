package com.opencrowd.monitor.cli.functions;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.springframework.context.ApplicationContext;

import com.opencrowd.monitor.managers.SystemStatusManager;
import com.opencrowd.monitor.model.Status;

/**
 * Gets our overall system status
 * @author jbramlett
 */
public class SystemStatusFunction extends AbstractFunction {
	private static final String kFunction = "systemStatus";
	
	protected SystemStatusManager systemStatusManager;
	
	/**
	 * Constructor
	 */
	public SystemStatusFunction() {
		super(kFunction);
	}

	/* (non-Javadoc)
	 * @see com.opencrowd.monitor.cli.functions.AbstractFunction#buildOptions()
	 */
	@Override
	protected Options buildOptions() {
        Options options = new Options();
        options.addOption("help", false, "This message");
        
        return options;
    }

	/* (non-Javadoc)
	 * @see com.opencrowd.monitor.cli.functions.AbstractFunction#parseCommandLine(org.apache.commons.cli.CommandLine)
	 */
	@Override
	protected void parseCommandLine(CommandLine commandLine) {
	}

	/* (non-Javadoc)
	 * @see com.opencrowd.monitor.cli.functions.AbstractFunction#isValid()
	 */
	@Override
	public boolean isValid() {
		return systemStatusManager != null;
	}

	/* (non-Javadoc)
	 * @see com.opencrowd.monitor.cli.functions.AbstractFunction#initialize(org.springframework.context.ApplicationContext)
	 */
	@Override
	public void initialize(ApplicationContext applicationContext) {		
		super.initialize(applicationContext);
		systemStatusManager = findBean(applicationContext, "systemStatusManager", SystemStatusManager.class);
	}

	/* (non-Javadoc)
	 * @see com.opencrowd.monitor.cli.functions.AbstractFunction#internalExecute()
	 */
	@Override
	public void internalExecute() throws Exception {
		Status status = systemStatusManager.getSystemStatus();
		println(status.toString());
	}

}
