package com.opencrowd.monitor.exception;

/**
 * Exception used when a given JSON document is not valid
 * @author jbramlett
 */
public class InvalidDocumentException extends Exception {
	private static final long serialVersionUID = -775153264253306039L;

	/**
	 * Constructor
	 */
	public InvalidDocumentException() {
	}

	/**
	 * Constructor
	 * @param message
	 */
	public InvalidDocumentException(String message) {
		super(message);
	}

	/**
	 * Constructor
	 * @param cause
	 */
	public InvalidDocumentException(Throwable cause) {
		super(cause);
	}

	/**
	 * Constructor
	 * @param message
	 * @param cause
	 */
	public InvalidDocumentException(String message, Throwable cause) {
		super(message, cause);
	}
}
