package com.opencrowd.monitor.heartbeat.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Receiver of our heartbeat messages
 * @author jbramlett
 */
public class HeartbeatReceiver {
	private static final Logger logger = LoggerFactory.getLogger(HeartbeatReceiver.class);

	/**
	 * Constructor
	 */
	public HeartbeatReceiver() {
	}

	
	
	/**
	 * Invoked when a new message is received from our topic
	 * @param message The incoming message
	 */
	public void receiveMessage(String message) {
		logger.info("Received <" + message + ">");
	}
}
