package com.opencrowd.monitor.managers;

import org.springframework.stereotype.Component;

import com.opencrowd.monitor.model.Status;

import java.util.Random;

/**
 * Manager responsible for getting our system status
 * @author jbramlett
 */
@Component
public class SystemStatusManager {

	private static final String[] AVAILABLE_STATUS = new String[] {"Red", "Yellow", "Green"};
	private Random random = new Random(System.currentTimeMillis());

	/**
	 * Constructor
	 */
	public SystemStatusManager() {
	}

	/**
	 * Gets our system status
	 * @return Status The system status
	 */
	public Status getSystemStatus() {
		return new Status(AVAILABLE_STATUS[random.nextInt(AVAILABLE_STATUS.length)]);
	}
}
