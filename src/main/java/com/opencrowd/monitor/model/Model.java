package com.opencrowd.monitor.model;

import com.opencrowd.monitor.util.JsonUtil;

/**
 * Base class for all model elements
 * @author jbramlett
 */
public abstract class Model {

	/**
	 * Constructor
	 */
	public Model() {		
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return JsonUtil.toJsonStringQuiet(this);
	}
}
