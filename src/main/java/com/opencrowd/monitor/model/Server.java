package com.opencrowd.monitor.model;

import org.joda.time.DateTime;
import org.joda.time.Seconds;

/**
 * Value object used to hold information about a particular server
 * 
 * @author jbramlett
 */
public class Server extends Model {
	protected DateTime registration;
	protected DateTime completion;
	protected DateTime latestHeartbeat;
	protected String serverId;
	

	/**
	 * Constructor
	 */
	public Server() {
		registration = new DateTime();
	}
	
	/**
	 * Constructor
	 * @param serverId
	 */
	public Server(String serverId) {
		this();
		this.serverId = serverId;
	}

	/**
	 * Registers a clean shutdown of this instance
	 */
	public void shutdown() {
		this.completion = new DateTime();
	}
	
	/**
	 * Registers a heartbeat from this server
	 */
	public void heartbeat() {
		latestHeartbeat = new DateTime();
	}
	
	/**
	 * Gets the time (in seconds) between the current time and our last heartbeat
	 * @return long The time in seconds
	 */
	public int getSecondsSinceHeartbeat() {
		if (latestHeartbeat != null) {
			return Seconds.secondsBetween(new DateTime(), latestHeartbeat).getSeconds();
		}
		else if (completion != null) {
			return Integer.MIN_VALUE;
		}
		else {
			return Integer.MAX_VALUE;
		}
	}
	
	/**
	 * Gets the total time this server has been up
	 * @return int The uptime in seconds
	 */
	public int getUptimeSeconds() {
		if (completion == null) {
			return 0;
		}
		else {
			return Seconds.secondsBetween(new DateTime(), registration).getSeconds();
		}
	}
	
	/**
	 * Gets our server id
	 * @return String The server id
	 */
	public String getServerId() {
		return serverId;
	}
}
