package com.opencrowd.monitor.model;

/**
 * Value object holding our system status
 * @author jbramlett
 */
public class Status extends Model {
	protected String status;

	/**
	 * Constructor
	 */
	public Status(String status) {
		this.status = status;
	}

	/**
	 * Gets the status
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}


	
}
