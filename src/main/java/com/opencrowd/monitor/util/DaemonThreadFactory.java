package com.opencrowd.monitor.util;


import java.util.concurrent.ThreadFactory;

/**
 * Factory used to create new daemon threads
 */
public class DaemonThreadFactory implements ThreadFactory {

	private int count = 1;

	public DaemonThreadFactory() {
	}

	/**
	 * Creates a new thread wrapping the given runnable
	 * @param r The runnable we are creating the new thread for
	 * @return Thread The new thread
	 */
	public Thread newThread(Runnable r) {
		Thread t = new Thread(r);
		t.setDaemon(true);
		t.setName("DaemonThread-" + count);
		count++;
		return t;
	}
}
