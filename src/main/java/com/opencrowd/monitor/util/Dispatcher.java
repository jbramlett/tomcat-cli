package com.opencrowd.monitor.util;

import org.springframework.stereotype.Component;
import java.util.concurrent.*;

/**
 * The dispatcher is used to dispatch work to threads in a non-blocking fashion
 */
@Component
public class Dispatcher {

	private ExecutorService executor = Executors.newFixedThreadPool(10, new DaemonThreadFactory());;

	/**
	 * Constructor
	 */
	public Dispatcher() {
	}


	/**
	 * Executes our given task
	 * @param r The runnable for the task to execute
	 */
	public void dispatch(Runnable r) {
		executor.execute(r);
	}
}
