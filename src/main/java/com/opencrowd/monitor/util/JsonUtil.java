package com.opencrowd.monitor.util;

import java.io.IOException;

import org.apache.commons.lang3.exception.ExceptionUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.opencrowd.monitor.exception.InvalidDocumentException;

/**
 * A collection of static methods used to handle some basic JSON related tasks - just
 * here to simplify some interactions
 * @author jbramlett
 */
public final class JsonUtil {

	private static ObjectMapper objectMapper = new ObjectMapper();
	
	static {
	    objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
	}
	
	/**
	 * Converts the given Object to a json representation
	 * @param obj The object we are converting
	 * @return String The json document
	 * @throws InvalidDocumentException
	 */
	public static String toJsonString(Object obj) throws InvalidDocumentException {
		try {
			return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
		}
		catch (JsonProcessingException jpe) {
			throw new InvalidDocumentException("Failed parsing to JSON!", jpe);
		}
	}
	
	/**
	 * Converts the given Object to a json representation
	 * @param obj The object we are converting
	 * @return String The json document
	 * @throws InvalidDocumentException
	 */
	public static String toJsonStringQuiet(Object obj)  {
        try {
            return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
        }
        catch (JsonProcessingException jpe) {
            return "Failed to parse JSON : " + jpe.getMessage() + "\n" + ExceptionUtils.getStackTrace(jpe);
        }
	}

	/**
	 * Converts the given ObjectNode to a JSON string
	 * @param objectNode The node is our root
	 * @return String The JSON String
	 * @throws InvalidDocumentException
	 */
	public static String toStringJson(ObjectNode objectNode) {
		try {
            return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(objectNode);
		}
		catch (JsonProcessingException jpe) {
			return null;
		}
	}
	
	/**
	 * Converts our given JSON string to a JsonNode
	 * @param doc The document we are converting
	 * @return JsonNode The node representation of the Json string
	 * @throws InvalidDocumentException
	 */
	public static JsonNode readJson(String doc)  throws InvalidDocumentException {
		try {
			return objectMapper.readTree(doc);
		}
		catch (JsonProcessingException jpe) {
			throw new InvalidDocumentException("Failed parsing JSON to nodes: " + doc, jpe);
		}
		catch (IOException ioe) {
			throw new InvalidDocumentException("Failed parsing JSON to nodes: " + doc, ioe);			
		}		
	}
	
	/**
	 * Converts our given object to a JsonNode
	 * @param o The document we are converting
	 * @return JsonNode The node representation of the Json string
	 */
	public static JsonNode readJson(Object o)   {
	    return objectMapper.valueToTree(o);
	}		

	/**
	 * Returns a flag indicating if the given string is a valid json string
	 * @param json The string we are validating
	 * @return boolean Returns true if the string is valid json, false otherwise
	 */
	public static boolean isValidJSON(String json) {
		boolean valid = false;
		
		try {
			if (json != null) {
				readJson(json);
				valid = true;
			}
		}
		catch (InvalidDocumentException ide) {
		}
		catch (Exception ee) {
		}
		return valid;
	}
		
	/**
	 * Clones the given json node creating a copy of it
	 * @param node The node we are cloning
	 * @return JsonNode The copy
	 */
	public static JsonNode clone(JsonNode node)  {
		try {
			if (node != null) {
				return readJson(toJsonString(node));
			}
		}
		catch (InvalidDocumentException ide) {
			// should never reach here since we are starting with
			// a JSON node
		}
		return null;
	}
	
	/**
	 * Constructor
	 */
	private JsonUtil() {
	}	
}