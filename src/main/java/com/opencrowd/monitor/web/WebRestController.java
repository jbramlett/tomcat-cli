package com.opencrowd.monitor.web;

import javax.servlet.http.HttpServletRequest;

import com.opencrowd.monitor.managers.SystemStatusManager;
import com.opencrowd.monitor.model.Status;
import com.opencrowd.monitor.util.Dispatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.context.request.async.DeferredResult;

/**
 * Rest controller defining the available set of url's that this instance supports
 * @author jbramlett
 */
@RestController
public class WebRestController {
	private static final Logger logger = LoggerFactory.getLogger(WebRestController.class);

	@Autowired
	protected SystemStatusManager systemStatusManager;

	@Autowired
	protected Dispatcher dispatcher;
	
	/**
	 * Constructor
	 */
	public WebRestController() {
	}

	
	/**
	 * Gets our system status as a color:
	 * 	red : means the system is in a bad state
	 * 	yellow : means the system is ok but there are potential problems
	 * 	green : means the system is up and running fine
	 * 
	 * @param request The incoming request
	 * @return Status The system status
	 */
	@RequestMapping(value="/systemstatus", method=RequestMethod.GET)
	public Status getSystemStatus(HttpServletRequest request) {
		logger.info("Received systemstatus request");
		return systemStatusManager.getSystemStatus();
	}


	/**
	 * Gets our system status as a color:
	 * 	red : means the system is in a bad state
	 * 	yellow : means the system is ok but there are potential problems
	 * 	green : means the system is up and running fine
	 *
	 * @param request The incoming request
	 * @return DeferredResult<Status> The system status
	 */
	@RequestMapping(value="/asyncsystemstatus", method=RequestMethod.GET)
	public DeferredResult<Status> getSystemStatusAsync(HttpServletRequest request) {
		logger.info("Received async systemstatus request");
		// Initiate the processing in another thread
		final DeferredResult<Status> deferredResult = new DeferredResult<Status>();

		dispatcher.dispatch(new Runnable() {
			public void run() {
				Status status = systemStatusManager.getSystemStatus();
				deferredResult.setResult(status);
			}
		});

		// Return to let go of the precious thread we are holding on to...
		return deferredResult;
	}

}
