package com.opencrowd.monitor.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Test case for our JsonUtil component
 * @author jbramlett
 */
@RunWith(JUnit4.class)
public class JsonUtilTest {
	
	/**
	 * Generates a map of Json data
	 * @return Map<String, Object> The Json map
	 */
	private Map<String, Object> getJsonMap() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("artist", "Pink Floyd");
		map.put("startYear", new Integer(1964));
		List<String> members = new ArrayList<String>();
		members.add("Syd Barrett");
		members.add("Roger Waters");
		members.add("Nick Mason");
		members.add("Richard Wright");
		members.add("David Gilmour");
		map.put("members", members);
		Map<String, Object> albums = new HashMap<String, Object>();
		Map<String, Object> dsotm = new HashMap<String, Object>();
		dsotm.put("year", new Integer(1974));
		dsotm.put("rating", new Integer(5));
		albums.put("Dark Side of the Moon", dsotm);	
		
		return map;
	}
	
	/**
	 * Gets our Json data with a nested doc
	 * @return JsonNode A Json node with a nested doc
	 */
	private JsonNode getJsonNodeWithNested() {
		ObjectNode doc = JsonNodeFactory.instance.objectNode();		
		doc.put("artist", "Pink Floyd");
		doc.put("startYear", new Integer(1964));
		
		ArrayNode members = JsonNodeFactory.instance.arrayNode();
		members.add("Syd Barrett");
		members.add("Roger Waters");
		members.add("Nick Mason");
		members.add("Richard Wright");
		members.add("David Gilmour");
		doc.putPOJO("members", members);

		ObjectNode albums = JsonNodeFactory.instance.objectNode();
		ObjectNode dsotm = JsonNodeFactory.instance.objectNode();
		dsotm.put("year", new Integer(1974));
		dsotm.put("rating", new Integer(5));
		albums.putPOJO("Dark Side of the Moon", dsotm);	
		
		doc.putPOJO("albums", albums);
		
		return doc;		
	}
	
	/**
	 * Tests converting a map to a Json string
	 * @throws Exception
	 */
	@Test
	public void testToJSONObject() throws Exception {
		Map<String, Object> jsonMap = getJsonMap();
		
		String json = JsonUtil.toJsonString(jsonMap);
		
		// now verify our JSON mapping - it is a little hacky here but should prove
		// sufficient - we verify we have an object, then verify we have both the element
		// for artist and our artist name, then verify we have members and a list
		assertTrue(json != null);
		assertTrue(json.startsWith("{"));
		assertTrue(json.endsWith("}"));
		assertTrue(json.contains("artist") && json.contains("Pink Floyd"));
		assertTrue(json.contains("members") && json.contains("[") && json.contains("]"));
	}

	/**
	 * Tests getting a Json string from an object node
	 * @throws Exception
	 */
	@Test
	public void testToJSONObjectNode() throws Exception {
		String json = JsonUtil.toJsonString(getJsonNodeWithNested());
		
		// now verify our JSON mapping - it is a little hacky here but should prove
		// sufficient - we verify we have an object, then verify we have both the element
		// for artist and our artist name, then verify we have members and a list
		assertTrue(json != null);
		assertTrue(json.startsWith("{"));
		assertTrue(json.endsWith("}"));
		assertTrue(json.contains("artist") && json.contains("Pink Floyd"));
		assertTrue(json.contains("members") && json.contains("[") && json.contains("]"));
		assertEquals(3, StringUtils.countMatches(json, "{"));
		assertEquals(3, StringUtils.countMatches(json, "}"));
	}
        
    /**
	 * Tests validating json
	 */
	@Test
	public void testValidate() {
		assertFalse(JsonUtil.isValidJSON("text_autoc,client,name"));
		assertTrue(JsonUtil.isValidJSON(JsonUtil.toJsonStringQuiet(getJsonNodeWithNested())));
	}
	
    /**
     * Tests clone
     */
    @Test
    public void testClone() {
        ObjectNode src = JsonNodeFactory.instance.objectNode();
        src.put("field1", "value1");
        src.put("field2", "value2");
        
        ObjectNode clone = (ObjectNode)JsonUtil.clone(src);
        clone.put("field3", "value3");

        src.put("field4", "value4");
        
        assertFalse("Found field that should not be there", src.has("field3"));
        assertFalse("Found field that should not be there", clone.has("field4"));
    }

    /**
     * Tests clone with a null
     */
    @Test
    public void testCloneNull() {
        assertNull(JsonUtil.clone(null));
    }
    
    /**
     * Tests reading Json from a string
     */
    @Test
    public void testReadJsonString() throws Exception {
    	String json = "{\"field\" : \"value\"}";
    	
    	JsonNode jsonNode = JsonUtil.readJson(json);
    	
    	// now verify our results
    	assertTrue(jsonNode.has("field"));
    	assertEquals("value", jsonNode.get("field").asText());
    }
    
    /**
     * Tests reading an object to a string
     * @throws Exception
     */
    public void testReadJsonObject() throws Exception {
    	JsonObject sample = new JsonObject("fieldName", "valueName");
    	
    	JsonNode jsonNode = JsonUtil.readJson(sample);
    	
    	// now verify the results
    	assertTrue(jsonNode.has("field"));
    	assertEquals("fieldName", jsonNode.get("field").asText());
    	assertTrue(jsonNode.has("value"));
    	assertEquals("valueName", jsonNode.get("value").asText());
    }
    
    class JsonObject {
    	protected String field;
    	protected String value;
    	
    	public JsonObject() {
    	}
    	public JsonObject(String field, String value) {
    		this.field = field;
    		this.value = value;
    	}
		public String getField() {
			return field;
		}
		public void setField(String field) {
			this.field = field;
		}
		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}
    	
    }
}
